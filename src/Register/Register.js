import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { useForm } from 'react-hook-form'

class Register extends Component {

    constructor(props) {
        super(props);
        this.state={
            username:'',
            password:'',
            repeat:''
        };
        this.handleChangeUsername = this.handleChangeUsername.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleChangeRepeat = this.handleChangeRepeat.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeUsername(event) {
        this.setState({username: event.target.value});
    }

    handleChangePassword(event) {
        this.setState({password: event.target.value});
    }

    handleChangeRepeat(event) {
        this.setState({repeat: event.target.value});
    }

    handleSubmit(event) {
        alert('Отправленное имя: ' + this.state.username +
            ' Отправленный пароль: ' + this.state.password +
            ' Повторный пароль: ' + this.state.repeat);
        event.preventDefault();
    }


    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="container">
                    <h1>Register</h1>
                    <p>Please fill in this form to create an account.</p>
                    <hr />

                        <label htmlFor="email"><b>Email</b></label>
                        <input type="text" placeholder="Enter Email" name="email" id="email"
                               onChange={this.handleChangeUsername} required />

                            <label htmlFor="psw"><b>Password</b></label>
                            <input type="password" placeholder="Enter Password" name="psw" id="psw"
                                   onChange={this.handleChangePassword} required />

                                <label htmlFor="psw-repeat"><b>Repeat Password</b></label>
                                <input type="password" placeholder="Repeat Password" name="psw-repeat" id="psw-repeat"
                                      onChange={this.handleChangeRepeat} required />
                                    </div>
                <button type="submit" className="registerbtn">Register</button>

                <div className="container signin">
                    <p>Already have an account? <Link to="/Login/">Sign in</Link>.</p>
                </div>
            </form>
        )
    }
}

export default Register;