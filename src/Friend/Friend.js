import React from 'react'
import { Link } from 'react-router-dom'
import { IUser} from "../types/user";

const Friend: React.FC = (props) => {

    React.useEffect(() => {
        void userGet()
    }, [])

    const [user, setUser] = React.useState({
        id: 1,
        login: "",
        password: "",
        friends: []
    });

    const userGet = React.useCallback(async() => {
        const response = await fetch(`http://localhost:3000/users/${props.id}`);
        const data = await response.json();
        setUser(data);
    }, [])

    return (
        <Link to={`/chat/${user.id}`}>
            <div className="container"><h1 className="friend-text"> {user.login}</h1></div>
        </Link>
    )
};

export default Friend;