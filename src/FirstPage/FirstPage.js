import React, {Component} from 'react'
import { Link } from 'react-router-dom'

class FirstPage extends Component {
    render() {
        return (
            <div className='FirstPage'>
                <hr className='hrFP'/>
                <Link to='/Login/'>
                    <h1 className="link">Login</h1>
                </Link>

                <Link to='/Register/'>
                    <h1 className="link">Register</h1>
                </Link>
            </div>
        )
    }
}

export default FirstPage;