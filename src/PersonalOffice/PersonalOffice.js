import React from 'react'
import {Link} from 'react-router-dom'
/*import { IUser } from "../types/user"*/
import Friend from '../Friend'
import { IUser } from "../types/user";


const PersonalOffice: React.FC = () => {


    React.useEffect(() => {
        void userGet()
    }, [])

    const [user, setUser] = React.useState({
        id: 0,
        login: "",
        password: "",
        friends: []
    })
    const userGet = React.useCallback(async () => {
        const response = await fetch (
            `http://localhost:3000/users/${window.location.pathname.split('/')[2]}`
            /*`http://localhost:3000/users/1`*/
        )
        const data = await response.json()
        console.log(data)
        setUser(data)
    }, [])


    if (!user) {
        return (
            <header>Error</header>
        )
    }

    return (
        <div className='office'>
            <h1 className="link">Chats</h1>
            <h1 className="link">login: {user.login}</h1>
            <div>
                {user.friends.map((friend, index) => <Friend key={index} id={friend}>{friend}</Friend>)}
            </div>
        </div>
    )

};

export default PersonalOffice;