import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import FirstPage from "./FirstPage";
import Login from "./Login";
import Chat from "./Chat"
import Register from "./Register";
import PersonalOffice from "./PersonalOffice"
import './App.css';


const App: React.FC = () => {
    return (
        <Router>
            <Route path="/" exact component={FirstPage} />
            <Route path="/Login/" exact component={Login} />
            <Route path="/Register/" exact component={Register} />
            <Route path="/PersonalOffice/:id?" exact component={PersonalOffice} />
            <Route path="/Chat/:id?" exact component={Chat} />
        </Router>
    )
}

export default App;
