import React, { Component } from 'react'
import {Link} from "react-router-dom";

/*class Login extends Component {

    constructor(props) {
        super(props);
        this.state={
            username:'',
            password:''
        };
        this.handleChangeUsername = this.handleChangeUsername.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
    }

    handleChangeUsername(event) {
        this.setState({username: event.target.value});
    }

    handleChangePassword(event) {
        this.setState({password: event.target.value});
    }

    handleSubmit(event) {
        alert('Отправленное имя: ' + this.state.username +
              ' Отправленный пароль ' + this.state.password);
        event.preventDefault();
    }*/

const Login: React.FC = (props) => {

    const [login, setLogin] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [users, setUsers] = React.useState([]);
    React.useEffect(() => {
        void usersGet()
    }, []);
    const usersGet = React.useCallback(async() => {
        const response = await fetch('http://localhost:3000/users');
        const data = await response.json();
        setUsers(data);
    }, []);

    const onSubmit = React.useCallback(
        (event) => {
            if (users) {
                console.log(users, login, password)
                users.map(function (user) {
                        console.log(user)
                        if (user.login === login && user.password === password) {
                            window.open(`/PersonalOffice/${user.id}`)
                        }
                    }
                )
            }
            alert('incorrect data')
        }, [login, password])


    return (
            <form onSubmit={onSubmit}>
                <div className="container">
                    <h1>Sign in</h1>
                    <p>Please fill in this form to log in.</p>
                    <hr />

                    <label htmlFor="email"><b>Email</b></label>
                    <input type="text" placeholder="Enter Email" name="email" id="email"
                           onChange={(event => {setLogin(event.target.value)})} required />
                    <label htmlFor="psw"><b>Password</b></label>
                    <input type="password" placeholder="Enter Password" name="psw" id="psw"
                           onChange={(event => {setPassword(event.target.value)})} required />
                </div>

                <button type="submit" className="registerbtn">Log in</button>
            </form>
    )

}

export default Login;