import React from 'react'


const Chat: React.FC = () => {


    React.useEffect(() => {
        void chatGet()
    }, [])

    const [chat, setChat] = React.useState({
        id: 0,
        messages: []
    })
    const chatGet = React.useCallback(async () => {
        const response = await fetch (
            `http://localhost:3000/chats/${window.location.pathname.split('/')[2]}`
        )
        const data = await response.json()
        console.log(data)
        setChat(data)
    }, [])


    if (!chat) {
        return (
            <header>Error</header>
        )
    }

    return (
        <div className='office'>
            <h1 className="link">Chat</h1>
            <h1 className="link">login: {chat.id}</h1>
            <div>
                {chat.messages.map((message, index) => <h1>{message}</h1>)}
            </div>
        </div>
    )

};

export default Chat;